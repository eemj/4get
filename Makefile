.PHONY: test build clean lint

test:
	python test/ && rm -rf downloads/

build:
	cargo build --release

clean:
	rm -rf target/ downloads/

lint:
	cargo clippy -- -D warnings
