pipeline {
    agent {
        dockerfile {
            filename "Dockerfile"
            label "master"
            args "-u 0:0 --entrypoint=''"
        }
    }

    triggers {
        cron("0 23 * * *")
    }

    options {
        disableConcurrentBuilds()
        buildDiscarder(logRotator(numToKeepStr: "1"))
        timeout(time: 25, unit: "MINUTES")
        timestamps()
    }

    stages {
        // Build before downloading the components,
        // so if the buliding fails we"ll save on our internet speeds.
        stage("Building") {
            steps {
                sh "make build"
            }
        }

        stage("Linting") {
            steps {
                sh "make lint || echo 'Linting failed, proceeding on with the build.'"
            }
        }

        stage("Testing") {
            steps {
                sh "make test"
            }
        }
    }

    post {
        always {
            cleanWs()
        }
    }
}
