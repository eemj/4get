extern crate num_cpus;
extern crate reqwest;

mod args;
mod posts;

use posts::Post;
use reqwest::{
    header::{HeaderMap, HeaderValue, USER_AGENT},
    Client,
};
use std::{
    env, fs, io,
    path::{Path, PathBuf},
    sync::{
        atomic::{AtomicUsize, Ordering},
        mpsc::channel,
        Arc, Mutex,
    },
    thread,
    time::Duration,
};

/// Holding the status for our workpool
struct Status {
    current: AtomicUsize,
    done: AtomicUsize,
}

impl Status {
    fn new() -> Self {
        Self {
            current: AtomicUsize::new(0),
            done: AtomicUsize::new(0),
        }
    }

    fn get(&self) -> (usize, usize) {
        (
            self.current.load(Ordering::Relaxed),
            self.done.load(Ordering::Relaxed),
        )
    }
}

struct Workpool {
    client: Arc<Client>,
    path: Arc<PathBuf>,
    status: Arc<Status>,
}

impl Clone for Workpool {
    fn clone(&self) -> Self {
        Self {
            client: Arc::clone(&self.client),
            path: Arc::clone(&self.path),
            status: Arc::clone(&self.status),
        }
    }
}

const DOWNLOADS_DIR: &str = "./downloads";
const THREAD_RATIO: f32 = 1.0;

fn build_client() -> Arc<Client> {
    // Initialize our headers, and reqwest client.
    let mut default_headers = HeaderMap::new();
    default_headers.insert(
        USER_AGENT,
        HeaderValue::from_static(
            "Mozilla/5.0 (X11; Linux x86_64; rv:61.0) Gecko/20100101 Firefox/61.0",
        ),
    );

    // Create a thread-safe client which will be used later.
    Arc::new(
        Client::builder()
            .timeout(Duration::from_secs(10))
            .default_headers(default_headers)
            .build()
            .expect("Unable to build client."),
    )
}

fn ensure_path_exists(path: PathBuf) -> Result<(), io::Error> {
    if !path.as_path().exists() {
        return fs::create_dir_all(path);
    }
    Ok(())
}

macro_rules! die {
    ($($tt:tt)*) => {
        eprintln!($($tt)*);
        std::process::exit(1);
    }
}

fn main() {
    // Getting the board and the thread number
    // Example: Let's say the user inputs "http://i.4cdn.org/ck/1538858452357.jpg",
    // it'll get translated into Thread { Board: "ck", Number: 1538858452357 }.
    let args = env::args()
        .nth(1)
        .expect("A thread uri wasn't specified.")
        .to_ascii_lowercase();

    let thread = args::extract_args(&args);

    let client = build_client();

    if ensure_path_exists(PathBuf::from(DOWNLOADS_DIR)).is_err() {
        die!("Unable to create the downloads path.");
    };

    // Getting the posts, if we don't find anything we'll initialize an empty vector.
    let posts = posts::get_posts(&client, &thread.uri()).unwrap_or_else(Vec::new);

    if posts.is_empty() {
        die!("There's nothing to download.");
    }

    // After we get the posts, we create the directory for the board.
    if ensure_path_exists(
        Path::new(DOWNLOADS_DIR)
            .join(&thread.board)
            .join(&thread.number.to_string()),
    )
    .is_err()
    {
        die!("Unable to create board directory.");
    }

    let thread_count = (num_cpus::get() as f32 / THREAD_RATIO).round() as usize;

    let total = posts.len();
    let queue = Arc::new(Mutex::from(posts.into_iter()));
    let status = Arc::new(Status::new());
    let path = Arc::new(
        PathBuf::from(DOWNLOADS_DIR)
            .join(&thread.board)
            .join(&thread.number.to_string()),
    );

    let (queue_tx, queue_rx) = channel::<Post>();
    {
        let status = Arc::clone(&status);
        let board = thread.board.clone();
        let workpool = Workpool {
            client,
            status,
            path,
        };
        thread::spawn(move || {
            while let Ok(post) = queue_rx.recv() {
                let uri = post.uri(&board);
                let Workpool {
                    path,
                    status,
                    client,
                } = workpool.clone();
                let post_name = format!("/{}/{}.{}", board, post.filename, post.ext);
                thread::spawn(move || {
                    // If we find the file, verify it's checksum with the API's md5, otherwise
                    // download.
                    let checksum = posts::post_exists(&uri, &path);
                    if checksum.is_some() && checksum.unwrap() == post.md5 {
                        println!("Found '{}'", post_name);
                    } else {
                        println!("Downloading '{}'", post_name);
                        posts::get_media(&client, &uri, &path);
                    }
                    // Update the status
                    status.current.fetch_sub(1, Ordering::SeqCst);
                    status.done.fetch_add(1, Ordering::SeqCst);
                });
            }
        });
    }

    loop {
        let (current, done) = (*status).get();

        if current == thread_count {
            continue;
        } else if total == done {
            break;
        }

        if let Some(queue_item) = (*queue).lock().expect("Unable to lock the queue.").next() {
            status.current.fetch_add(1, Ordering::SeqCst);
            queue_tx
                .send(queue_item)
                .expect("Unable to store post to queue.");
        }
    }
}
