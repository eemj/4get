pub struct Thread {
    pub board: String,
    pub number: u64,
}

impl Thread {
    pub fn uri(&self) -> String {
        format!(
            "http://a.4cdn.org/{}/thread/{}.json",
            self.board, self.number
        )
    }
}

pub fn extract_args(uri: &str) -> Box<Thread> {
    if !uri.contains('/') {
        panic!("You passed an invalid uri.");
    }

    let split: Vec<String> = uri
        .split('/')
        .filter(|item| {
            !(item.is_empty()
                || item.starts_with("http")
                || item.starts_with("board")
                || item.starts_with("thread"))
        })
        .map(String::from)
        .collect();

    Box::new(Thread {
        board: split[0].to_owned(),
        number: split[1]
            .parse::<u64>()
            .expect("Unable to convert string to u64."),
    })
}
