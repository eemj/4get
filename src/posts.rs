extern crate base64;
extern crate md5;
extern crate serde_json;

use self::serde_json::Value;
use reqwest::Client;
use std::{fs::File, io::Write, path::PathBuf};

#[derive(Debug)]
pub struct Post {
    pub md5: String,
    pub filename: u64,
    pub ext: String,
}

impl Post {
    /// Generates a post URI with the provided board.
    pub fn uri(&self, board: &str) -> String {
        format!(
            "http://i.4cdn.org/{board}/{filename}.{ext}",
            board = board,
            filename = self.filename,
            ext = self.ext
        )
    }
}

impl Clone for Post {
    fn clone(&self) -> Self {
        Self {
            md5: self.md5.clone(),
            filename: self.filename,
            ext: self.ext.clone(),
        }
    }
}

/// Gets the posts found in the thread and returns an optioned boxed Posts structs with the useful
/// details.
pub fn get_posts(client: &Client, uri: &str) -> Option<Vec<Post>> {
    let mut response = client.get(uri).send().expect("Unable to retrieve posts.");
    let body = response.text().expect("Unable to retrieve posts text.");
    let parsed: Value = serde_json::from_str(&body).expect("Unable to unmarshal json.");
    let posts = parsed
        .get("posts")
        .expect("Unable to get posts object from body.")
        .as_array()
        .expect("Expected `posts` to be an array.");

    if posts.is_empty() {
        return None;
    }

    let mut filenames: Vec<Post> = Vec::new();
    for post in posts {
        if !(post["tim"].is_null() && post["ext"].is_null()) {
            filenames.push(Post {
                ext: post["ext"]
                    .as_str()
                    .expect("Unable to convert file `extension` to string.")[1..]
                    .to_string(),
                filename: post["tim"]
                    .as_u64()
                    .expect("Unable to convert file `name` to integer."),
                md5: post["md5"]
                    .as_str()
                    .expect("Unable to convert file `md5` to string.")
                    .to_string(),
            });
        }
    }

    Some(filenames)
}

/// Gets the filename from the URI.
pub fn filename_from_uri(uri: &str) -> &str {
    uri.split('/')
        .collect::<Vec<&str>>()
        .last()
        .expect("Unable to retrieve the last element from the URI")
}

/// Checks if the file exists, and if it exists it returns it's md5 base64'd hash.
pub fn post_exists(uri: &str, path: &PathBuf) -> Option<String> {
    let path = path.join(filename_from_uri(uri));
    if path.as_path().exists() {
        let contents = std::fs::read(path).expect("Unable to read the files contents.");
        let computation = md5::compute(&contents).0;
        return Some(base64::encode(&computation));
    }
    None
}

/// Gets the media's contents and writes them to a file
pub fn get_media(client: &Client, uri: &str, path: &PathBuf) {
    let path = path.join(filename_from_uri(uri));
    let mut response = client.get(uri).send().expect("Unable to retrieve media.");
    let mut buffer: Vec<u8> = Vec::new();
    response
        .copy_to(&mut buffer)
        .expect("Unable to copy bytes to vector.");
    let mut file: Box<File> = Box::new(File::create(path).expect("Unable to create file."));
    file.write_all(&buffer).expect("Unable to write to file.");
}
