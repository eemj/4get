import os
import requests
import json
import subprocess

board = "wg"

if __name__ == "__main__":
    response = requests.request(
        "GET", url="https://a.4cdn.org/%s/catalog.json" % board)

    if response.status_code != 200:
        raise Exception("Status Code != 200")

    body = json.loads(response.text)

    # FIXME; Indeed we didn't do any validation, but it should work.
    threads = body[0]["threads"]
    thread = threads[len(threads) - 1]["no"]

    response = requests.request(
        "GET", url="https://a.4cdn.org/%s/thread/%s.json" % (board, thread))

    if response.status_code != 200:
        raise Exception("Status Code != 200")

    body = json.loads(response.text)

    posts = [
        "{}{}".format(post["tim"], post["ext"]) for post in body["posts"]
        if "tim" in post and "ext" in post
    ]

    uri = "http://boards.4chan.org/%s/thread/%s" % (board, thread)

    subprocess.call("cargo run %s" % uri, shell=True)

    download_path = os.path.join(os.getcwd(),
                                 "downloads/%s/%s" % (board, thread))

    print("Ensuring the path %s exists" % download_path)

    assert (os.path.exists(download_path))

    found_files = [
        f for f in os.listdir(download_path)
        if os.path.isfile(os.path.join(download_path, f))
    ]

    has_missing = False
    for post in posts:
        if post in found_files:
            print("[✓] %s" % post)
        else:
            print("[✗] %s" % post)
            has_missing = True

    if has_missing:
        print("There are missing posts.")
    else:
        print("Success!")

    assert has_missing is False
