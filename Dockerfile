FROM ubuntu:16.04

COPY . /srv/app

WORKDIR /srv/app

ENV \
    RUSTUP_HOME=/usr/local/rustup \
    CARGO_HOME=/usr/local/cargo \
    PATH=/usr/local/cargo/bin:$PATH

RUN \
    apt-get update && apt-get upgrade -y && \
    apt-get install -y \
        build-essential \
        curl \
        git \
        python \
        python-pip \
        openssl \
        libssl-dev \
        pkg-config && \
    curl -sSf https://sh.rustup.rs/ | sh -s -- -y && \
    . $CARGO_HOME/env && \
    rustup toolchain install nightly stable && \
    rustup default stable && \
    rustup component add clippy-preview && \
    pip install -r requirements.txt

ENTRYPOINT ["/bin/bash"]
